
NAME := rust-for-the-recalcitrant-cpp-programmer


all:
	echo "try 'make upload'"

upload:
	rsync -r \
		--exclude .git \
		./ \
		dreamhost:artificialworlds.net/presentations/${NAME}/
