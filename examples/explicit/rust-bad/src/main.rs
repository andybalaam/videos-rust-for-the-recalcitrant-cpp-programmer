struct MyResource {
    frob_count: usize,
}

impl MyResource {
    fn new() -> Self {
        Self { frob_count: 6 }
    }

    fn stats(&self) -> usize {
        self.frob_count
    }
}

struct MyService {
    res: Option<MyResource>,
}

impl MyService {
    fn shutdown(&mut self) {
        self.res = None;
    }

    fn gather_stats(&self) -> usize {
        // ERROR: method not found in `Option<MyResource>
        self.res.stats()
    }
}

fn main() {
    println!("# ownership\n");

    println!("* Creating a service");
    let mut my_service = MyService {
        res: Some(MyResource::new()),
    };

    println!("* Shutting the service down");
    my_service.shutdown();

    println!("* Oops... polling for stats after shutdown");
    println!("{}", my_service.gather_stats());
}
