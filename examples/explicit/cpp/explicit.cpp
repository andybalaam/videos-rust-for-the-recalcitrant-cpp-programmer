#include <assert.h>
#include <iostream>
#include <memory>

using namespace std;

class MyResource {
private:
    int frob_count_ = 6;
public:
    int stats() {
        return frob_count_;
    }
};

class MyService {
public:
    unique_ptr<MyResource> res_;

    void shutdown() {
        res_ = nullptr;
    }

    int gather_stats() {
        // Undefined behaviour if my_resource_.get() == null
        return res_->stats();
    }
};

int main() {
    cout << "# explicit\n\n";

    cout << "* Creating a service\n";
    MyService my_service{unique_ptr<MyResource>(new MyResource)};

    cout << "* Shutting the service down\n";
    my_service.shutdown();

    cout << "* Oops... polling for stats after shutdown\n";
    cout << my_service.gather_stats() << "\n";
}
