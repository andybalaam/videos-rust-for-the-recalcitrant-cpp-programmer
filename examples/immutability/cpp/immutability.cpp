#include <iostream>
#include <memory>

using namespace std;

struct Child {
    virtual void set_age(int age) = 0;
    virtual int get_age() = 0;
};

struct MyChild: public Child {
    int age_;

    MyChild(int age): age_(age) {}

    virtual void set_age(int age) {
        age_ = age;
    }

    virtual int get_age() {
        return age_;
    }
};

struct Parent {
    unique_ptr<Child> child;
};

int main() {
    cout << "# immutability-cpp\n\n";

    cout << "* Creating a const Parent+MyChild\n";
    const Parent parent{unique_ptr<Child>(new MyChild{1})};

    cout << "* Modifying it!\n";
    parent.child->set_age(10);

    cout << "* It was modified\n";
    cout << parent.child->get_age() << "\n";
}
