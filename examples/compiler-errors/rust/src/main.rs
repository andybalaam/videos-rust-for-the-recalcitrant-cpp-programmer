fn main() {
    let mut abc = String::from("abc");
    let ab = &abc[0..2];
    abc.push_str("DEF");
    println!("{ab}");
}

/*
error[E0502]: cannot borrow `abc` as mutable because it is also borrowed as immutable
 --> src/main.rs:4:5
  |
3 |     let ab = &abc[0..2];
  |               --- immutable borrow occurs here
4 |     abc.push_str("DEF");
  |     ^^^^^^^^^^^^^^^^^^^ mutable borrow occurs here
5 |     println!("{ab}");
  |                -- immutable borrow later used here

For more information about this error, try `rustc --explain E0502`.
error: could not compile `compiler-errors-rust` due to previous error
*/
