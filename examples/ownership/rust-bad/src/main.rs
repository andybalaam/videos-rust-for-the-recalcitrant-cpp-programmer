struct MyObject {
    x: usize,
}

fn main() {
    println!("# ownership\n");

    println!("* Creating an object");
    let obj = MyObject { x: 3 };

    println!("* Moving it into a vector");
    let mut vec = Vec::new();
    vec.push(obj);

    println!("* Oops... using it after move!");
    // Error: value borrowed here after move
    println!("{}", obj.x);
}
