fn main() {
    let result = do_something_tricky2(2);
    match result {
        Ok(answer) => println!("Success: {}", answer),
        Err(_e) => println!("FAILURE."),
    }
}

fn do_something_tricky(input: i32) -> Result<i32, String> {
    tricky_thing(input)?;
    second_thing();
    Ok(input * 2)
}

fn do_something_tricky2(input: i32) -> Result<i32, String> {
    let res = tricky_thing(input);
    match res {
        Err(e) => return Err(e),
        Ok(r) => r,
    };
    second_thing();
    Ok(input * 2)
}

fn tricky_thing(input: i32) -> Result<(), String> {
    if input > 100 {
        Err(String::from("Too big!"))
    } else {
        Ok(())
    }
}

fn second_thing() {
    println!("second_thing");
}
