enum IpAddress {
    V4([u8; 4]),
    V6([u16; 6]),
}

fn main() {
    let addr1 = IpAddress::V4([10, 0, 0, 1]);
    let addr2 = IpAddress::V6([0x10, 0, 0, 0, 0, 0x1]);

    process(addr1);
    process(addr2);
}
fn process(addr: IpAddress) {
    match addr {
        IpAddress::V4(a) => println!("{}.{}.{}.{}", a[0], a[1], a[2], a[3]),
        IpAddress::V6(b) => println!(
            "{:x}:{:x}:{:x}:{:x}:{:x}:{:x}",
            b[0], b[1], b[2], b[3], b[4], b[5]
        ),
    }
}
