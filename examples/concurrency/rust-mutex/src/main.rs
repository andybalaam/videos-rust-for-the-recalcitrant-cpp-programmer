use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    println!("# concurrency-rust-mutex\n");

    println!("* Launching worker threads");
    let mut threads = Vec::new();
    let counter = Arc::new(Mutex::new(0));
    for _i in 0..10000 {
        let c = counter.clone();
        threads.push(thread::spawn(move || *c.lock().unwrap() += 1));
    }

    println!("* Waiting for workers to finish");
    for thread in threads {
        thread.join().expect("Join failed");
    }

    println!("Total: {}", *counter.lock().unwrap());
}
