use std::thread;

fn main() {
    let mut threads = Vec::new();
    let mut counter = 0;
    for _i in 0..10000 {
        threads.push(thread::spawn(|| counter += 1));
    }
    for thread in threads {
        thread.join().expect("Join failed");
    }
    println!("Total: {}", counter);
}
