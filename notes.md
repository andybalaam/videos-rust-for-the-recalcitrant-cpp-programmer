# Notes

## Proposal

"Andy teaches CB about neat features of Rust and CB tries to show that they have
what they need in C++."

Description: "Andy has been working in Rust and is excited to share his
experience of all the positive features that he has found useful.

CB has decades of experience with C++ and years of experience in other languages
and is sceptical of the need to invest time in another language. They have
reluctantly agreed to "hear Andy out" and are confident that they have
everything which they need in C++ to tackle the same sort of problems that these
Rust features are supposed to help with.

Andy is looking forward to demonstrating ownership, the type system,
immutability, pattern matching, safe concurrency, helpful compiler messages and,
worryingly for CB, the package ecosystem."

## Format

CB asks a question/states that some feature is not needed.

Andy explains the feature.

CB gives their thoughts and a short discussion follows.

CB responds with a yes/no as to whether this is a useful feature.

## Topics

- the type system
  - being explcit
  - macros make common patterns easy
- ownership
- immutability
- enum types and pattern matching (and error handling?)
- safe concurrency
- compiler messages
- the package ecosystem

## Concepts

- most assignments are move, and move operations are automatically provided
- some objects are always-copy
- you can mark objects as unmoveable (Unpin), and say when you need them to be
  not-moved (because you're taking a pointer to them) with Pin
- Owning vs. referring-to are explicit, so the compiler can enforce sensible
  rules about e.g. no dangling refs.

## The type system: explicit handling of all cases

See examples/explicit - in the C++ we see a common situation: MyService has
a shutdown method that cleans up some resources, and a long-running
stats-grathering thread polls it after it has been shut down. This results an
invalid read.

The rust-bad example fails to compile because we were forced by the compiler
to put the MyResource inside an Option<> wrapper, and when we use it we are
forced to handle the case that it might be empty.

Your friends who do Haskell are always telling you "if it compiles, it's
correct" and now you can have that feeling in a language with a similar
execution model to C++.

## The type system: common patterns made easy

See examples/common-patterns/

C++ requires writing a lot of code and e.g. the Rule of Three.

Rust provides utilities - the example shows how deriving the Clone, Debug and
Serialize traits give you automatic behaviour very conveniently.

But Rust also enforces correct patterns e.g. to clone a Vec the items must be
Clone.

Side note: (almost) everything is move (or Copy), so you can put anything into a
Vec. (In fact, you can mark something as not-move - it's called Unpin. It's used
for self-referential data structures such as Futures, but otherwise quite rare.

## Ownership

See examples/ownership

In Rust, one variable owns each value, and the compiler enforces it.

Others can borrow it - either many immutably, or one mutably. You can't pass
around an invalid reference, do a use-after-free, or have something change under
you. As the example shows, you can't use something after you've moved it.

## Immutability

See examples/immutability

It's a property of the variable, and it goes all the way down. Both of these are
Right.

In the C++ example I can modify parent even though it's const.

In Rust, I can't. I don't rely on the type to prevent modification - it's the
variable.

## Enum types and pattern matching (and error handling)

It's incredibly useful to be able to say something is either A or B. I can't
remember how I coped without it.

It's also a great way of avoiding inheritance, and being more direct in how we
write code.

See examples/enum-types/rust-ip - we can represent two types of IP address in
one type and use a match statement to find out which it is when you need to.

Great way to handle errors.

See examples/enum-types/rust-errors - We can return Result and we are always
explicit about what could happen. The ? operator gives us a shortcut to return
an error if this expression results in an error.

Key to emphasise: the ? is pure syntactic sugar: there are no exceptions - we
just return an Err value.

## Safe concurrency

See examples/concurrency

It's very easy to write code with data races in C++.

In Rust it's a compile error, and mutexes etc. are available to do it right.

I give 2 examples: Mutexes and channels. When I tested with large numbers I
could not see much difference in performance.

The mutex example is a direct way to solve the problem: locking the value we
want to change, but mutexes come with their own probems e.g. deadlocks if you
take them in the wrong order, and Rust won't protect you from that.

The channels example doesn't use any explicit locks, and allows each thread to
go at its own pace.

## Compiler messages

Some of the concepts in Rust are hard, but the compiler really tries to help.

Not only does it lay out errors in a clear way, it also contains lots of
heuristic rules to suggest what the fix might be, and gives pointers to further
information.

The linter ("clippy") suggests good style. `cargo fmt` formats into a standard
style.

## The package ecosystem

One standard way of specifying what dependencies you need.

One repo containing many libraries, including many high-quality ones.

Everything including documentation gets downloaded as source and built locally.

Packages specify what dependency versions they need and this is resolved.
Multiple versions of a single package can be used without conflict if needed.

The upshot: adding a dependency is a single command and it just works.

No signing (yet?)

No protection against e.g. typo-squatting.

Some core-ish functionality e.g. JSON is in widely-trusted crates, not stdlib.

https://blessed.rs for recommended crates.
